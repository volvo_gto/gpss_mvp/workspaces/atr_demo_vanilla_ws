# atr_demo_vanilla_ws (Vanilla)

This repo contains the git submodules to test the ATR simulator packages. It includes several ROS2 packages as git-submodules.

## Description

This repo will create a ros2 workspace with the needed ros packages to run the ATR simulator.

The ROS2 packages included in this workspace are:

<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_controller/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_demo/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_description/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_driver/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_factory_state/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_joy/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_nona/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_path_generator/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_trajectory_generator/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/control/atr_utils/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/ar_tag_msgs.git/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_camera_state_msgs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_error_msgs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_formation_msgs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_human_msgs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_job_msgs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_object_msgs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_path_msgs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_predicted_object_msgs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_signal_msgs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_srvs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/gpss_interfaces/atr_state_msgs/-/tree/vanilla>

<https://gitlab.com/volvo_gto/gpss_mvp/shared/factory_db/-/tree/vanilla>


## Dependencies

### C++

```bash
sudo apt install ros-galactic-xacro ros-galactic-joy nlohmann-json3-dev
```

### Python

```bash
pip install PyYAML
```

### Hardware

Some of the demos in this workspace use a joystick Logitech F710 (<https://www.logitech.com/sv-se/product/f710-wireless-gamepad>)

## Installation

We assume:

a) that you have already installed our BashTools (<https://gitlab.com/volvo_gto/gpss_mvp/shared/tools/bashtools>) and GitTools (<https://gitlab.com/volvo_gto/gpss_mvp/shared/tools/gittools>),

b) you have configured your SSH-Key in Gitlab. (<https://docs.gitlab.com/ee/user/ssh.html>),

c) and you have already installed ROS2 galactic (<https://docs.ros.org/en/galactic/Installation/Ubuntu-Install-Debians.html>)


Open a new Terminal (CTRL+ALT+T)

```bash
git clone --branch vanilla git@gitlab.com:volvo_gto/gpss_mvp/workspaces/atr_demo_vanilla_ws.git
cd atr_demo_vanilla_ws/
gitsiniupco vanilla
source /opt/ros/galactic/setup.bash
colcon build  --symlink-install --merge-install --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=1
```

After the installation you will have the following file structure in your workspace:

```bash
.
├── addModulesSimple.sh
├── docs
│   └── figures
│       └── mvp_ws_architecture_96.png
├── modules.txt
├── README.md
└── src
    ├── control
    │   ├── atr_controller
    │   ├── atr_demo
    │   ├── atr_description
    │   ├── atr_driver
    │   ├── atr_factory_state
    │   ├── atr_joy
    │   ├── atr_nona
    │   ├── atr_path_generator
    │   ├── atr_trajectory_generator
    │   └── atr_utils
    └── shared
        ├── factory_db
        └── gpss_interfaces
            ├── ar_tag_msgs
            ├── atr_camera_state_msgs
            ├── atr_error_msgs
            ├── atr_formation_msgs
            ├── atr_human_msgs
            ├── atr_job_msgs
            ├── atr_object_msgs
            ├── atr_path_msgs
            ├── atr_predicted_object_msgs
            ├── atr_signal_msgs
            ├── atr_srvs
            └── atr_state_msgs            
```

Where all the ros2 packages are git-submodules.

The architecture of communication of the nodes provided by this package is depicted in the following figure:

![ATR ws](docs/figures/mvp_ws_architecture_96.png)

## How to use

You can run the demos described in <https://gitlab.com/volvo_gto/gpss_mvp/control/atr_demo/-/blob/vanilla/README.md>

Each ros package has a README file with detailed information.
